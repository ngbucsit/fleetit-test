# Generated by Django 4.2.5 on 2023-10-09 09:46

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("tags", "0001_initial"),
        ("vehicles", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="vehicle",
            name="tags",
            field=models.ManyToManyField(to="tags.tag"),
        ),
    ]
