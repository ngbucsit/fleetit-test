from django.db import models
from tags.models import Tag


# Create your models here.
class Vehicle(models.Model):
    make = models.CharField(max_length=128)
    model = models.CharField(max_length=128)
    year = models.IntegerField()
    plate_number = models.CharField(max_length=32)
    tags = models.ManyToManyField(Tag, blank=True)
