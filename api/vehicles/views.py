from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from .models import Vehicle
from .serializers import VehicleSerializer

from tags.serializers import TagSerializer
from tags.models import Tag


# Create your views here.
class VehicleViewSet(viewsets.ModelViewSet):
    queryset = Vehicle.objects.all()
    serializer_class = VehicleSerializer
    filterset_fields = ["tags__id", "tags__name"]

    @action(methods=["get"], detail=True)
    def tags(self, request, pk=None):
        tags = self.get_object().tags.all()
        serializer = TagSerializer(tags, many=True)
        return Response(serializer.data)

    @tags.mapping.post
    def add_tag(self, request, pk=None):
        vehicle = self.get_object()
        serializer = TagSerializer(data=request.data)
        if serializer.is_valid():
            tags = Tag.objects.filter(name=serializer.validated_data["name"])

            if tags:
                vehicle.tags.add(tags.first())
                return Response(serializer.data, status=status.HTTP_201_CREATED)

            tag = serializer.save()
            vehicle.tags.add(tag)
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
