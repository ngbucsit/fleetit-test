from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from .models import Tag
from .serializers import TagSerializer

from vehicles.serializers import VehicleSerializer
from drivers.serializers import DriverSerializer


# Create your views here.
class TagViewSet(viewsets.ModelViewSet):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    filterset_fields = ["id", "name"]

    @action(methods=["get"], detail=True)
    def vehicles(self, request, pk=None):
        vehicles = self.get_object().vehicle_set.all()
        serializer = VehicleSerializer(vehicles, many=True)
        return Response(serializer.data)

    @action(methods=["get"], detail=True)
    def drivers(self, request, pk=None):
        drivers = self.get_object().driver_set.all()
        serializer = DriverSerializer(drivers, many=True)
        return Response(serializer.data)
