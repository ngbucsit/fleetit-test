from django.db import models

from tags.models import Tag
from vehicles.models import Vehicle


# Create your models here.
class Driver(models.Model):
    name = models.CharField(max_length=128)
    vehicles = models.ManyToManyField(Vehicle, through="DriverVehicle")
    tags = models.ManyToManyField(Tag, blank=True)


class DriverVehicle(models.Model):
    driver = models.ForeignKey(Driver, on_delete=models.CASCADE)
    vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE)
    date_driven = models.DateField()
