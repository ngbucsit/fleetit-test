from rest_framework import serializers

from .models import Driver, DriverVehicle


class DriverVehicleSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = DriverVehicle


class DriverSerializer(serializers.ModelSerializer):
    vehicles = DriverVehicleSerializer(
        source="drivervehicle_set", many=True, read_only=True
    )

    class Meta:
        fields = "__all__"
        model = Driver
