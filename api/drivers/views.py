from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from .models import Driver, DriverVehicle
from .serializers import DriverSerializer, DriverVehicleSerializer

from tags.serializers import TagSerializer
from tags.models import Tag


# Create your views here.
class DriverViewSet(viewsets.ModelViewSet):
    queryset = Driver.objects.all()
    serializer_class = DriverSerializer
    filterset_fields = ["tags__id", "tags__name"]

    @action(methods=["get"], detail=True)
    def tags(self, request, pk=None):
        tags = self.get_object().tags.all()
        serializer = TagSerializer(tags, many=True)
        return Response(serializer.data)

    @tags.mapping.post
    def add_tag(self, request, pk=None):
        driver = self.get_object()
        serializer = TagSerializer(data=request.data)
        if serializer.is_valid():
            tags = Tag.objects.filter(name=serializer.validated_data["name"])

            if tags:
                driver.tags.add(tags.first())
                return Response(serializer.data, status=status.HTTP_201_CREATED)

            tag = serializer.save()
            driver.tags.add(tag)
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(methods=["get"], detail=True)
    def vehicles(self, request, pk=None):
        driver_vehicles = self.get_object().drivervehicle_set.all()
        serializer = DriverVehicleSerializer(driver_vehicles, many=True)
        return Response(serializer.data)

    @vehicles.mapping.post
    def add_vehicle(self, request, pk=None):
        driver = self.get_object()
        request.data["driver"] = driver.id
        serializer = DriverVehicleSerializer(data=request.data)
        if serializer.is_valid():
            driver_vehicle = serializer.save()
            driver.drivervehicle_set.add(driver_vehicle)
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(
        methods=["delete"], detail=True, url_path=r"vehicles/(?P<vehicle_id>[^/.]+)"
    )
    def remove_vehicle(self, request, pk=None, vehicle_id=None):
        driver_vehicle = DriverVehicle.objects.filter(vehicle__id=vehicle_id)
        if driver_vehicle:
            driver_vehicle.delete()
            return Response(
                data="Successfully removed vehicle", status=status.HTTP_200_OK
            )

        return Response(
            data="Failed to remove vehicle", status=status.HTTP_400_BAD_REQUEST
        )


class DriverVehicleViewSet(viewsets.ModelViewSet):
    queryset = DriverVehicle.objects.all()
    serializer_class = DriverVehicleSerializer
